# Cloud Deploy

Docker images for deployments on major cloud platforms (GCP, Azure, AWS)

### :warning: Deprecation warning

The `registry.gitlab.com/gitlab-org/cloud-deploy:latest` Docker image is deprecated.
You can still pull it, but it will not be updated, and there will be no official support
for it.

Update your CI pipeline configuration to use one of the following images instead:

- `registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest` to use AWS CLI commands.
- `registry.gitlab.com/gitlab-org/cloud-deploy/aws-ecs:latest` to deploy your application to AWS ECS.

### AWS

Instructions to pull our AWS Docker images can be found in the project's
[Releases page](https://gitlab.com/gitlab-org/cloud-deploy/-/releases).

---

Documentation: https://docs.gitlab.com/ee/ci/cloud_deployment

---

| Service  | Implemented  |  Details |
|-------|-----|---|
| AWS   | :heavy_check_mark:  | AWS CLI (version 2.x)  |
| Azure | :x:  |   |
| GCP   | :x:  |   |

The following projects depend on `cloud-deploy`:
- [Status Page](https://gitlab.com/gitlab-org/status-page)
- [GitLab](https://gitlab.com/gitlab-org/gitlab) (bundled CI/CD templates) (as of [GitLab 12.9](https://gitlab.com/gitlab-org/gitlab/issues/207962))

### About project releases

This project leverages [`release-cli`](https://gitlab.com/gitlab-org/release-cli/) to create releases automatically as part of its CI process.

To trigger a new pipeline which will create a new release, you just need to
create a new tag:
- Create [a new tag](https://gitlab.com/gitlab-org/cloud-deploy/-/tags/new):
  - In `Tag name`, enter a new version, eg. `v0.1.2`.
  - Leave the `Message` text area empty, as we want to create a [lightweight tag](https://git-scm.com/book/en/v2/Git-Basics-Tagging/) here.
  - Leave the `Release notes` text area empty too, as the pipeline will create
    that content automatically.

Upon creation of this tag, a new pipeline will start and will create the new
related release.
